<?php

if( ! defined('NANO_VERSION') || NANO_VERSION < 2 ) {
    throw new Exception('Nano Framework version 2.x is required.');
}

use Nano\Component\Loader as Loader;
use Pagebuilder\Model\Partial as Partial;
use Pagebuilder\Helper\Config as Config;

(new Loader)->addPrefix('Pagebuilder', JPATH_ADMINISTRATOR . '/components/com_pagebuilder')->register();

// Get item
$item = $params->get('id', -1);
$classes = $params->get('classes', '');

// Get partial
$partial = (new Partial)->findOrFail($item);

foreach($partial->body->partial as $element) {

	if ( $classes !== '' && property_exists($element->data, 'classes') ) {
		$element->data->classes = $classes;
	}

	Config::make()->render($element->type, ['element' => $element, 'position' => 'module']);

}